#ifndef __FOC_LIB__
#define __FOC_LIB__

#include <Arduino.h>
#include "FOC.h"
namespace FOC
{
    typedef struct
    {
        char name[10];
        int act_pos; //电机实际位置
        int last_pos;//电机上一时刻的位置
        int act_speed;//电机实时速度

        int Kp;
        int Ki;
        int Kd;

        float q;
        float d;
        float c;

        float A;
        float B;
        float C;
        
        float alpha;
        float beta;
        
        int motor_speed;
        int dir;

        bool en_motor; //电机使能
        bool state_encoder;//编码器状态
    } sMotor;
    
}
#endif